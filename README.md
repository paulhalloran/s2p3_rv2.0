# README for installing, setting up and running s2p3_rv2.0
The instructions here assume you are working on a linux machine

############################################################
# Requirements                                             #
############################################################


- git (https://gist.github.com/derhuerst/1b15ff4652a867391f03)
  - install with:

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git

############################################################
# Installation                                             #
############################################################

Note you may want to speed things up by greating a RAM disk and making the temporary location to hold the unzipped met data this.

e.g.
sudo mkdir /mnt/ramdisk
sudo mount -t tmpfs -o size=3g tmpfs /mnt/ramdisk
then your temp location is  /mnt/ramdisk:

clone this repository to your computer

git clone https://paulhalloran@bitbucket.org/paulhalloran/s2p3_rv2.0.git

change directory into the s2p3_rv2.0 directory

cd s2p3_rv2.0/main

#compile code

gfortran -Ofast -o s2p3_rv2.0 s2p3_rv2.0.f90

s2p3_rv2.0 should now be a working executable

#copying in the forcing data

copy in the forcing files you created using the code and instructions here https://bitbucket.org/paulhalloran/s2p3_rv2.0_forcing. e.g.:

if s2p3_rv2.0_forcing is in example_location_1 (e.g.) ~/Documents
and s2p3_rv2.0 is in example_location_2 (e.g.) username@remote_cluster:~/

scp example_location_1/s2p3_rv2.0_forcing/s12_m2_s2_n2_h_map.dat example_location_2/s2p3_rv2.0/domain

scp example_location_1/s2p3_rv2.0_forcing/initial_nitrate.dat example_location_2/s2p3_rv2.0/domain

scp -r example_location_1/s2p3_rv2.0_met_data/met_data_*.tar.gz example_location_2/my_runs_met_data

#running the model

before running you will need to edit at least one line in:

s2p3_rv2.0/main/run_map_parallel.py

Here you must change the line 'base_directory = ...' to point to the s2p3_rv2.0 directory on your computer

You will also probably have to edit the 'met_data_location = ' to point to the locatino you have copied the tar.gz met files to (example_location_2/my_runs_met_data in the example above)

You may also have to create the tempoarry directory which will hold the untarred and gziped met files (e.g. met_data_temporary_location = base_directory+'met/spatial_data/')

You may well also want to change the number or processors, output file names etc. here

Under the heading 'Variables to output from model' you can select which variables to output from the model, see the model output section below.

then run with either:

python2.7 run_map_parallel.py

OR if you are running on a cluster/supercomputer with a batch system, you may be able to use a runscript like runscript_parallel and submit with something like:

msub runscript_parallel

#model output

The model output will be in the directory specified wiuthin run_map_parallel.py, with the default output file being output_map
This contains in columns, left to right: day number, longitude, latitude, then the variables you have specified in run_map_parallel.py in the order specified under the heading 'Variables to output from model'.

Note changing this will necessitate changes to the processing into netcdf script here https://bitbucket.org/paulhalloran/s2p3_rv2.0_processing


############################################################
# Processing/plotting the output                           #
############################################################

see https://bitbucket.org/paulhalloran/s2p3_rv2.0_processing

Scripts and instructions are provided to convert the output into netcdf format and plot the output


############################################################
# Points to note                                           #
############################################################

############################################################
# Updates                                                  #
############################################################

To avoid overly hot SSTs in some tropical areas, changes being made to prescribe downwelling shortwave and longwave radiation. The net downward shortwave at the surface is used directly by the model, but the longwave requires a minor change to the model code. I've changed:
hl=0.985*5.67d-8*(surf_temp**4.0)*(0.39-0.05*vap**0.5)*(1.0-0.6d-4*cloud(idmet)**2.0)	!original codes Longwave
to
hl=0.985*5.67d-8*(surf_temp**4.0)-lw_rad_down0	!Longwave
note, I'm not sure why humiditry would have been used in tgeh original calculation. Is this a way at getting towards skin temperature from tghe model's surface level temperature, or accounting for absorbtion by non-cloud water vapour?
